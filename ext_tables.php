<?php

/*  | This extension is made with ❤ for TYPO3 CMS and zazudesign.
 *  | It is licensed under GNU General Public License.
 *  |
 *  | (c) 2019-2024 Armin Vieweg <armin@v.ieweg.de>
 */

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'form_serialnumber',
    'Configuration/TypoScript',
    'EXT:form Serialnumber Finisher'
);
